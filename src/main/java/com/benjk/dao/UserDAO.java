package com.benjk.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.benjk.model.Collection;
import com.benjk.model.User;

public class UserDAO {
	
	public static void insertUser(User u) throws Exception {
		try (Connection connect = ConnectionDAO.connect()) {
			try (Statement statement = connect.createStatement()) {
				String rqt = "INSERT INTO User (email, nom, prenom, password, birthday) "
						+ "VALUES ('" + u.getEmail()+ "', '" + u.getNom() + "', '" + u.getPrenom()
						+ "', '" + u.getPassword()+ "', '"+ u.getBirthday()+"')";
				statement.executeUpdate(rqt);
				System.out.println("INSERT USER OK !!!");
			}
			ConnectionDAO.disconnect(connect);
		}

	}
	
	public static User getUser(String email) throws Exception{
		User myUser = null;
		try (Connection connect = ConnectionDAO.connect()) {
			try (Statement statement = connect.createStatement()) {
				String rqt = "SELECT * FROM User WHERE email='"+email+"'";
				ResultSet rS = statement.executeQuery(rqt);
				if(rS.next()) {
					myUser = new User(rS.getString("nom"), rS.getString("prenom"), rS.getString("password"), rS.getString("birthday"), rS.getString("email"));
				}
				System.out.println("SELECT OK !!!");
			}
			ConnectionDAO.disconnect(connect);
		}
		return myUser;
	}
}
