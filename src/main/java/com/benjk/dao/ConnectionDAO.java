package com.benjk.dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnectionDAO {
	private static String url, login, password;
	
	public static Connection connect() throws Exception {
		Properties props = new Properties();
		try(FileInputStream fis =new FileInputStream("C:\\Users\\Formation\\eclipse-workspace\\monBeauSite\\conf.properties")){
			props.load(fis);
		}

		Class.forName(props.getProperty("jdbc.driver.class"));
		url = props.getProperty("jdbc.url");
		login = props.getProperty("jdbc.login");
		password = props.getProperty("jdbc.password");
		
		Connection connection = DriverManager.getConnection(url, login , password);
		System.out.println("CONNECT BDD OK");
		return connection;			
	}
	
	public static void disconnect(Connection connection) throws SQLException {
		System.out.println("DISCONNECT BDD OK");
		connection.close();
	}
	
}
