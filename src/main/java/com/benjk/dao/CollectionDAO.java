package com.benjk.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.benjk.model.Collection;

public class CollectionDAO {

	public static void insertCollection(Collection c) throws Exception {
		try (Connection connect = ConnectionDAO.connect()) {
			try (Statement statement = connect.createStatement()) {
				String rqt = "INSERT INTO Collection (nom, description, imageUrl) VALUES ('" + c.getNom()
						+ "', '" + c.getDescription() + "', '" + c.getUrlImage() + "')";
				statement.executeUpdate(rqt);
				System.out.println("INSERT OK !!!");
			}
			ConnectionDAO.disconnect(connect);
		}

	}
	
	public static List<Collection> getCollections() throws Exception{
		List<Collection> collections = new ArrayList();
		try (Connection connect = ConnectionDAO.connect()) {
			try (Statement statement = connect.createStatement()) {
				String rqt = "SELECT * FROM Collection";
				ResultSet rS = statement.executeQuery(rqt);
				while(rS.next()) {
					collections.add(new Collection(rS.getString("imageUrl"), rS.getString("nom"), rS.getString("description")));
				}
				System.out.println("SELECT OK !!!");
			}
			ConnectionDAO.disconnect(connect);
		}
		return collections;
	}
	
	public static void delete(Collection c) throws Exception {
		try (Connection connect = ConnectionDAO.connect()) {
			try (Statement statement = connect.createStatement()) {
				String rqt = "DELETE FROM Collection WHERE (nom='"+c.getNom()+"' AND description='"+c.getDescription()+"' AND imageUrl='"+c.getUrlImage()+"');";
				System.out.println(rqt);
				statement.executeUpdate(rqt);
				System.out.println("DELETE OK !!!");
			}
			ConnectionDAO.disconnect(connect);
		}
	}	

}
