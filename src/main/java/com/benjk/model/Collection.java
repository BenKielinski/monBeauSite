package com.benjk.model;

public class Collection {
	private String urlImage;
	private String nom, description;
	
	public Collection() {

	}

	public Collection(String urlImage, String nom, String description) {
		super();
		this.urlImage = urlImage;
		this.nom = nom;
		this.description = description;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	
}
