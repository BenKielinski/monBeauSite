package com.benjk.model;

public class User implements AutoCloseable{
	private String nom, prenom, password;
	private String birthday;
	private String email;
	
	public User(){
	}

	public User(String nom, String prenom, String password, String birthday, String email) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.password = password;
		this.birthday = birthday;
		this.email = email;
	}

	public User(String prenom, String password, String email) {
		super();
		this.prenom = prenom;
		this.password = password;
		this.email = email;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return prenom + " " + nom + ", " + password + " mail : " + email;
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub		
	}
	
	
}
