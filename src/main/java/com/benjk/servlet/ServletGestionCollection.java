package com.benjk.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.benjk.dao.CollectionDAO;
import com.benjk.model.Collection;

/**
 * Servlet implementation class ServletGestionCollection
 */
@WebServlet("/admin")
public class ServletGestionCollection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<Collection> collections;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletGestionCollection() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			collections = CollectionDAO.getCollections();
			request.setAttribute("collections", collections);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id = (Integer.parseInt(request.getParameter("formid")))-1;
		try {
			System.out.println(collections);
			CollectionDAO.delete(collections.get(id));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doGet(request, response);
	}

}
