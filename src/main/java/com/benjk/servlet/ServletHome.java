package com.benjk.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.benjk.dao.UserDAO;
import com.benjk.model.User;

/**
 * Servlet implementation class ServletHome
 */
@WebServlet("")
public class ServletHome extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletHome() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub		
		User myUser = null;
		try {
			myUser = (User) request.getSession().getAttribute("user");
			System.out.println(myUser);
			if(myUser==null) {				
				myUser = UserDAO.getUser((String) request.getSession().getAttribute("user"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(myUser!=null && myUser.getPrenom()!="") {
			request.setAttribute("prenom", myUser.getPrenom());
		}else {
			request.setAttribute("prenom", "inconnu");
		}
		this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		request.getSession().setAttribute("logout", false);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
