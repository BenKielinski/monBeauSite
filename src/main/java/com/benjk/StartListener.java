package com.benjk;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.benjk.dao.CollectionDAO;
import com.benjk.model.Collection;

@WebListener
public class StartListener implements ServletContextListener{

	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
//		List<Collection> collections = new ArrayList();
//		collections.add(new Collection("7.jpg", "Miaou", "Pozay dans mon hamac"));
//		collections.add(new Collection("4.jpg", "Mistigri", "La chatte de la voisne"));
//		collections.add(new Collection("6.jpg", "Les chats sosies", "De Francis Chatbrel à Chatdolf Hitler"));
//		collections.add(new Collection("3.JPG", "Chats & chiens", "Section Interracial"));
		try {
			arg0.getServletContext().setAttribute("collections", CollectionDAO.getCollections());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		try {
//			System.out.println("LISTENER OK");
//			CollectionDAO.insertCollection(collections.get(2));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}

}
