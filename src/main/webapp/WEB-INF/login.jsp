<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>CONNECTEZ-VOUS</h1>
	<c:if test="${wrongID}">
		<p style="color:red">Mauvais identifants</p>
	</c:if>
	<form name="formul" method="post" action="http://localhost:8080/monBeauSite/login">
        <label for="email">Rentrez votre email</label><input type="text" name="email" >
        <br>
        <label  for="password">Rentrez votre mot de passe</label><input type="password" name="password" >
        <br>
        <button type="submit">Envoyer</button>
    </form>
</body>
</html>