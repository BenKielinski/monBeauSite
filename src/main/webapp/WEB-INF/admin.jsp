<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Site de Benjamin</title>
<style>
#oeuvresParTheme {
	display: flex;
	flex-flow: wrap;
	width: 100%;
	color: white;
	margin-bottom: 5px;
}

#oeuvresParTheme .tOConteneur {
	position: relative;
	width: calc(50vw - 15px * 2);
	padding-top: 15%;
	padding-bottom: 15%;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-flow: column;
	overflow: hidden;
	margin: 5px;
	border-radius: 1%;
}

@media screen and (max-width: 700px) {
	#oeuvresParTheme .tOConteneur {
		width: calc(100vw - 15px * 1.25);
	}
}

#oeuvresParTheme .tOConteneur img {
	position: absolute;
	width: 100%;
	height: 100%;
	object-fit: cover;
	z-index: -1;
}

#oeuvresParTheme .tOConteneur h2 {
	font-size: 2.5em;
}

#oeuvresParTheme .tOConteneur h4 {
	font-size: 1.8em;
}

#oeuvresParTheme .tOConteneur .exploreDiv {
	border: 2px white solid;
	padding: 5px;
	color: white;
	margin-bottom: 15px;
	margin-top: 15px;
}

#oeuvresParTheme .tOConteneur:hover {
	transform: scale(0.99);
}

#oeuvresParTheme .tOConteneur:hover .exploreDiv {
	border: 2px black solid;
	background-color: white;
	color: black;
}
</style>
</head>
<body>
<div style="text-align:center">
</div>
	<div id=oeuvresParTheme>
		<c:forEach items="${ collections }" var="collection" varStatus="status">
			<div class=tOConteneur>
				<img src="${collection.urlImage}">
				<h2>${collection.nom}</h2>
				<h4>${collection.description}</h4>
				<form method=post action="http://localhost:8080/monBeauSite/admin">
					<button type=submit>Delete</button>
					<input type=hidden name="formid" value="${status.count}">
				</form>
			</div>
		</c:forEach>		
	</div>
</body>
</html>