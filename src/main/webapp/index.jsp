<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Site de Benjamin</title>
<style>
#oeuvresParTheme {
	display: flex;
	flex-flow: wrap;
	width: 100%;
	color: white;
	margin-bottom: 5px;
}

#oeuvresParTheme .tOConteneur {
	position: relative;
	width: calc(50vw - 15px * 2);
	padding-top: 15%;
	padding-bottom: 15%;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-flow: column;
	overflow: hidden;
	margin: 5px;
	border-radius: 1%;
}

@media screen and (max-width: 700px) {
	#oeuvresParTheme .tOConteneur {
		width: calc(100vw - 15px * 1.25);
	}
}

#oeuvresParTheme .tOConteneur img {
	position: absolute;
	width: 100%;
	height: 100%;
	object-fit: cover;
	z-index: -1;
}

#oeuvresParTheme .tOConteneur h2 {
	font-size: 2.5em;
}

#oeuvresParTheme .tOConteneur h4 {
	font-size: 1.8em;
}

#oeuvresParTheme .tOConteneur .exploreDiv {
	border: 2px white solid;
	padding: 5px;
	color: white;
	margin-bottom: 15px;
	margin-top: 15px;
}

#oeuvresParTheme .tOConteneur:hover {
	transform: scale(0.99);
}

#oeuvresParTheme .tOConteneur:hover .exploreDiv {
	border: 2px black solid;
	background-color: white;
	color: black;
}
</style>
</head>
<body>
<div style="text-align:center">
	<h1>Bienvenue ${ prenom }</h1>	
	<c:if test="${prenom eq'inconnu'}">
	<h1>Inscrivez-vous pour profiter d'incroyables photos de chats</h1>
	<button onclick="window.location.href='http://localhost:8080/monBeauSite/inscription'">
		INSCRIPTION</button><br>
	<button onclick="window.location.href='http://localhost:8080/monBeauSite/login'">
		LOGIN</button>
	</c:if>
	<c:if test="${prenom ne'inconnu'}">
		<h1>Votre num�ro bancaire n'est pas valide, vous n'avez pas acc�s aux chats</h1>
		<button onclick="window.location.href='http://localhost:8080/monBeauSite/logout'">
		LOGOUT</button>
	</c:if>
	<c:if test="${logout}">
		<p style="color:red">Vous avez �t� deconnect�</p>
	</c:if>
	
</div>
	<div id=oeuvresParTheme>
		<c:forEach items="${ collections }" var="collection">
			<div class=tOConteneur>
				<img src="${collection.urlImage}">
				<h2>${collection.nom}</h2>
				<h4>${collection.description}</h4>
				<span class="exploreDiv">D�couvrir la collection</span>
			</div>
		</c:forEach>		
	</div>
</body>
</html>